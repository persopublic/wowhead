package WowHead;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class WowHeadSteps {

    WebDriver driver;

    @Given("je suis sur le site de wowhead")
    public void je_suis_sur_le_site_https_www_wowhead_com_fr_wow() {
        System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.wowhead.com/wow");
    }
    @Given("j ai accepte les cookies")
    public void j_ai_accepte_les_cookies() {
        driver.findElement(By.id("onetrust-accept-btn-handler")).click();
    }
    
    @When("je clique sur {string}")
    public void je_clique_sur(String string) {
        driver.findElement(By.xpath("//span[contains(text(),\""+string+"\")]")).click();
    }
    
    @Then("je peux voir la vue d'ensemble du patch {double} \\(overview)")
    public void je_peux_voir_la_vue_d_ensemble_du_patch_overview(Double double1) {
        WebElement titre = driver.findElement(By.xpath("//h1[parent::section]"));
        assertTrue("le titre n'est pas bon.",titre.getText().contains(String.format("Patch %s",double1)));
        driver.close();
        driver.quit();
    }

    @When("je tape un nom dans la barre de recherche {string}")
    public void je_tape_un_nom_dans_la_barre_de_recherche(String string) {
        driver.findElement(By.xpath("//input[parent::form]")).sendKeys(string);
    }

    @When("je clique sur la premiere proposition")
    public void je_clique_sur_la_premiere_proposition() {
        driver.findElement(By.xpath("//span[preceding-sibling::div[@data-icon=\"boss\"] and @class=\"result-name\"]")).click();
    }

    @Then("je peux voir sa localisation a {string}")
    public void je_peux_voir_sa_localisation_a(String string) {
        String localisation = driver.findElement(By.xpath("//div[span[@id=\"locations\"]]")).getText();
        System.out.println(localisation);
        assertTrue("la localisation n'est pas bonne ou présente",localisation.contains(string));
    }

    @Then("je peux consulter l encadrer Quick fact")
    public void je_peux_consulter_l_encadrer_Quick_fact() {
        WebElement infobox = driver.findElement(By.xpath("//table[@class=\"infobox\"]"));
        String infoboxText = infobox.getText();
        System.out.println(infoboxText);
        assertTrue("l'encadré n'est pas visible",infobox.isDisplayed());
        driver.close();
        driver.quit();
    }
}
