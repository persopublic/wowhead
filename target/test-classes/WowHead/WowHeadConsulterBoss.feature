Feature: consulter boss

  Scenario Outline:
    Given je suis sur le site de wowhead
    And j ai accepte les cookies
    When je tape un nom dans la barre de recherche <nomBoss>
    And je clique sur la premiere proposition
    Then je peux voir sa localisation a <localisation>
    And je peux consulter l encadrer Quick fact

    Examples:
      | nomBoss               | localisation                     |
      | "Fyrakk"              | "Amirdrassil, the Dream's Hope"  |
      | "Raszageth"           | "Vault of the Incarnates"        |
      | "Sarkareth"           | "Aberrus, the Shadowed Crucible" |
      | "Denathrius"          | "Castle Nathria"                 |
      | "Sylvanas Windrunner" | "Sanctum of Domination"          |
      | "Rygelon"             | "Sepulcher of the First Ones"    |
      | "Vexiona"             | "Ny'alotha, the Waking City"     |
      | "Jaina"               | "Battle of Dazar'alor"           |